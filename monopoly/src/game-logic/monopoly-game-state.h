#pragma once
#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>

struct MonopolyModel
{
public:

	MonopolyModel(const std::vector<std::string>& playerNames);

	int getActivePlayer();
	int getNumPlayers();

	const std::string& getPlayerName (int playerNumber);

	std::unique_ptr<boost::property_tree::ptree> state;
};
